﻿using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Infra.Data.EntitiesMappings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Infra.Data.Context
{
    public class RotasContext : DbContext
    {

        public DbSet<Ponto> Pontos { get; set; }
        public DbSet<Ligacao> Ligacoes { get; set; }
//        public DbSet<Rota> Rotas { get; set; }

        public RotasContext()
            : base("Rotas")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {                
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(10));

            modelBuilder.Configurations.Add(new PontoMapping());
            modelBuilder.Configurations.Add(new LigacaoMapping());
            //modelBuilder.Configurations.Add(new RotaMapping());

            base.OnModelCreating(modelBuilder);
        }

    }
}
