﻿using ABC.Rotas.Domain.Interfaces.Repositories;
using ABC.Rotas.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Infra.Data.Repository
{
    public class Repository<T> : IBaseRepository<T> where T : class
    {
        protected RotasContext Contexto;
        protected DbSet<T> DbSet;

        public Repository()
        {
            Contexto = new RotasContext();
            DbSet = Contexto.Set<T>();
        }
        public T Adicionar(T objeto)
        {
            var retorno = DbSet.Add(objeto);
            Contexto.SaveChanges();
            return retorno;
        }

        public IEnumerable<T> ObterTodos()
        {
            return DbSet.ToList();
        }

        public T ObterPorId(int Id)
        {
            return DbSet.Find(Id);
        }

        public T Atualizar(T objeto)
        {
            var entry = Contexto.Entry(objeto);
            DbSet.Attach(objeto);
            entry.State = EntityState.Modified;
            SaveChanges();
            return objeto;
        }

        public int Remover(int Id)
        {
            var entry = DbSet.Find(Id);
            if (entry == null)
                return 0;
            DbSet.Remove(entry);
            return SaveChanges();
        }

        public IEnumerable<T> Buscar(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public int SaveChanges()
        {
            return Contexto.SaveChanges();
        }

        public void Dispose()
        {
            Contexto.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
