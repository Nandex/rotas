﻿using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Infra.Data.Repository
{
    public class RotaRepository : Repository<Rota>, IRotaRepository
    {
    }
}
