﻿using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Infra.Data.Repository
{
    public class PontoRepository : Repository<Ponto>, IPontoRepository
    {
        public new Ponto Atualizar(Ponto objeto)
        {
            var ponto = Contexto.Pontos.Where(x => x.Id == objeto.Id).Single();
                ponto.Nome = objeto.Nome;
                ponto.X = objeto.X;
                ponto.Y = objeto.Y;

            var entry = Contexto.Entry(ponto);
            
            DbSet.Attach(ponto);
            
            entry.State = EntityState.Modified;
            
            SaveChanges();
            
            return objeto;
        }
    }
}
