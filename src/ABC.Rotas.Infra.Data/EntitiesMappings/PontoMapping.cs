﻿using ABC.Rotas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ABC.Rotas.Infra.Data.EntitiesMappings
{
    public class PontoMapping : EntityTypeConfiguration<Ponto>
    {
        public PontoMapping()
        {
            Property(p => p.Nome)
                .IsRequired()
                .HasColumnAnnotation("PontoNomeIndex", new IndexAnnotation(new IndexAttribute() { IsUnique = true }));

            Ignore(x => x.Erros);

            ToTable("Pontos");
        }
        
    }
}
