﻿using ABC.Rotas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Infra.Data.EntitiesMappings
{
    public class LigacaoMapping : EntityTypeConfiguration<Ligacao>
    {
        public LigacaoMapping()
        {
            Ignore(x => x.Erros);

            ToTable("Ligacoes");
        }
    }
}
