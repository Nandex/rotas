﻿using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Application.Servicos;
using ABC.Rotas.Domain.Interfaces.Repositories;
using ABC.Rotas.Domain.Interfaces.Services;
using ABC.Rotas.Domain.Services;
using ABC.Rotas.Infra.Data.Repository;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Infra.CrossCutting.Ioc
{
    public class RegistradorIoc
    {
        public static void RegisterServices(Container container)
        {
            container.Register<IPontoAppService, PontoAppService>(Lifestyle.Scoped);
            container.Register<ILigacaoAppService, LigacaoAppService>(Lifestyle.Scoped);
            container.Register<IRotaAppService, RotaAppService>(Lifestyle.Scoped);

            container.Register<IPontoService, PontoService>(Lifestyle.Scoped);
            container.Register<ILigacaoService, LigacaoService>(Lifestyle.Scoped);
            //container.Register<IRotaService, RotaService>(Lifestyle.Scoped);

            container.Register<IPontoRepository, PontoRepository>(Lifestyle.Scoped);
            container.Register<ILigacaoRepository, LigacaoRepository>(Lifestyle.Scoped);
            //container.Register<IRotaRepository, RotaRepository>(Lifestyle.Scoped);
        }
    }
}
