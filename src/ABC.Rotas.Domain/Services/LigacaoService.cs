﻿using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Domain.Interfaces.Repositories;
using ABC.Rotas.Domain.Interfaces.Services;
using ABC.Rotas.Domain.Validations;
using ABC.Rotas.Domain.Validations.LigacaoValidations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Services
{
    public class LigacaoService : ServiceBase<Ligacao>, ILigacaoService
    {
        private readonly ILigacaoRepository _ligacaoRepository;

        public LigacaoService(ILigacaoRepository ligacaoRepository) : base(ligacaoRepository)
        {
            _ligacaoRepository = ligacaoRepository;
        }

        public int Remover(int Id1, int Id2)
        {
            var ligacao = _ligacaoRepository.ObterTodos()
                     .Where(x => (x.P1_Id == Id1 && x.P2_Id == Id2) ||
                           (x.P1_Id == Id2 && x.P2_Id == Id1)).FirstOrDefault();

            if (ligacao == null)
                return 0;

            return _ligacaoRepository.Remover(ligacao.Id);
        }

        public new Ligacao Adicionar (Ligacao ligacao)
        {
            List<Validator<Ligacao>> ligacaoValidators = new List<Validator<Ligacao>>
            {
                new LigacaoJaExistenteValidator(_ligacaoRepository.ObterTodos().ToList()),
                new LigacaoValidator(),
            };
            ligacaoValidators.ForEach(x => x.Validar(ligacao));
            foreach(var erros in ligacaoValidators.Where(x => x.Erros.Count > 0))
            {
                erros.Erros.ForEach(e =>
                {
                    ligacao.Erros.Add(e);
                });
            }
            if (ligacaoValidators.Any(x => x.Erros.Count > 0))
                return ligacao;

            return _ligacaoRepository.Adicionar(ligacao);
        }
    
    }
}
