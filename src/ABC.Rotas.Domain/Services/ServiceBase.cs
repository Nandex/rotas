﻿using ABC.Rotas.Domain.Interfaces.Repositories;
using ABC.Rotas.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Services
{
    public class ServiceBase<T> : IBaseService<T> where T : class
    {
        private readonly IBaseRepository<T> _repositorio;

        public ServiceBase(IBaseRepository<T> repositorio)
        {
            _repositorio = repositorio;
        }

        public T Adicionar(T objeto)
        {
            return _repositorio.Adicionar(objeto);
        }

        public IEnumerable<T> ObterTodos()
        {
            return _repositorio.ObterTodos();
        }

        public T ObterPorId(int Id)
        {
            return _repositorio.ObterPorId(Id);
        }

        public T Atualizar(T objeto)
        {
            return _repositorio.Atualizar(objeto);
        }

        public int Remover(int Id)
        {
            return _repositorio.Remover(Id);
        }

        //public IEnumerable<T> Buscar(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        //{
        //    return _repositorio.Buscar(predicate);
        //}

        //public void Dispose()
        //{
        //    Dispose();
        //}

        //public int SaveChanges()
        //{
        //    return _repositorio.SaveChanges();
        //}
    }
}
