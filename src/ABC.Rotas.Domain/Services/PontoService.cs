﻿using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Domain.Interfaces.Repositories;
using ABC.Rotas.Domain.Interfaces.Services;
using ABC.Rotas.Domain.Validations;
using ABC.Rotas.Domain.Validations.PontoValidations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Services
{
    public class PontoService : ServiceBase<Ponto>, IPontoService
    {
        private readonly IPontoRepository _pontoRepository;

        public PontoService(IPontoRepository pontoRepository) : base(pontoRepository)
        {
            _pontoRepository = pontoRepository;
        }

        public new Ponto Adicionar(Ponto objeto)
        {
            List<Validator<Ponto>> pontoValidators = new List<Validator<Ponto>>
            {
                new PontoCoordenadaJaExistenteValidator(_pontoRepository.ObterTodos().ToList()),
                new PontoNomeJaExistenteValidator(_pontoRepository.ObterTodos().ToList()),
            };
            pontoValidators.ForEach(x => x.Validar(objeto));
            foreach(var erros in pontoValidators.Where(x => x.Erros.Count > 0))
            {
                erros.Erros.ForEach(e =>
                {
                    objeto.Erros.Add(e);
                });
            }
            if (pontoValidators.Any(x => x.Erros.Count > 0))
                return objeto;

            return _pontoRepository.Adicionar(objeto);
        }

        public new Ponto Atualizar(Ponto objeto)
        {
            var pontos = _pontoRepository.ObterTodos();

            List<Validator<Ponto>> pontoValidators = new List<Validator<Ponto>>
            {
                new PontoCoordenadaJaExistenteValidator(pontos.Where(x => x.Id != objeto.Id).ToList()),
                new PontoNomeJaExistenteValidator(pontos.Where(x => x.Id != objeto.Id).ToList()),
            };
            pontoValidators.ForEach(x => x.Validar(objeto));
            foreach(var erros in pontoValidators.Where(x => x.Erros.Count > 0))
            {
                erros.Erros.ForEach(e =>
                {
                    objeto.Erros.Add(e);
                });
            }
            if (pontoValidators.Any(x => x.Erros.Count > 0))
                return objeto;

            return _pontoRepository.Atualizar(objeto);
        }

    }
}
