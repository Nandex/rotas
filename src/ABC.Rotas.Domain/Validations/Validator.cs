﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Validations
{
    public abstract class Validator<T>
    {
        public List<string> Erros { get; protected set; }

        public Validator()
        {
            if (Erros == null)
                Erros = new List<string>();
        }

        protected virtual void IncluirErro(string mensagem)
        {
            Erros.Add(mensagem);
        }

        public virtual void Validar(T valor) { }
        public virtual void Validar() { }

    }
}
