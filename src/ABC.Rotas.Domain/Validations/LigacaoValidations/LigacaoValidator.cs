﻿using ABC.Rotas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Validations.LigacaoValidations
{
    public class LigacaoValidator : Validator<Ligacao>
    {
        public override void Validar(Ligacao valor)
        {
            if (valor.P1_Id <= 0)                
                IncluirErro("Não foi informado o ponto origem da ligação");
            if (valor.P2_Id <= 0)
                IncluirErro("Não foi informado o ponto destino da ligação");
        }
    }
}
