﻿using ABC.Rotas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Validations.LigacaoValidations
{    
    public class LigacaoJaExistenteValidator : Validator<Ligacao>
    {
        private List<Ligacao> Ligacoes { get; set; }
        public LigacaoJaExistenteValidator(List<Ligacao> ligacoes)
	    {
            Ligacoes = ligacoes;
	    }

        public override void Validar(Ligacao valor)
        {
            if (Ligacoes.Where(x => (x.P1_Id == valor.P1_Id && x.P2_Id == valor.P2_Id) || (x.P1_Id == valor.P2_Id && x.P2_Id == valor.P1_Id)).Count() > 0)
                IncluirErro("Já existe uma ligação entre os pontos informados");
        }

    }
}
