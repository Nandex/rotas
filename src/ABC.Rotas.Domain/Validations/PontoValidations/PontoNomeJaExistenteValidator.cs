﻿using ABC.Rotas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Validations.PontoValidations
{
    public class PontoNomeJaExistenteValidator : Validator<Ponto>
    {
        private List<Ponto> Pontos { get; set; }
        public PontoNomeJaExistenteValidator(List<Ponto> pontos)
        {
            Pontos = pontos;
        }

        public override void Validar(Ponto valor)
        {
            if (Pontos.Where(x => x.Nome == valor.Nome).Count() > 0)
                IncluirErro("Já existe um ponto com o nome informado.");
        }

    }
}
