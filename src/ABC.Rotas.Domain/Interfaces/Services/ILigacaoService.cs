﻿using ABC.Rotas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Interfaces.Services
{
    public interface ILigacaoService : IBaseService<Ligacao>
    {
        int Remover(int Id1, int Id2);
    }
}
