﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        T Adicionar(T objeto);
        IEnumerable<T> ObterTodos();
        T ObterPorId(int Id);
        T Atualizar(T objeto);
        int Remover(int Id);
        //IEnumerable<T> Buscar(Expression<Func<T, bool>> predicate);
        int SaveChanges();
    }
}
