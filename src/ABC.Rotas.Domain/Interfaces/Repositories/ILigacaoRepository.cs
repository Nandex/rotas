﻿using ABC.Rotas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Domain.Interfaces.Repositories
{
    public interface ILigacaoRepository : IBaseRepository<Ligacao>
    {
    }
}
