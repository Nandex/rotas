﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABC.Rotas.Domain.Entities
{
    public class Rota
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        protected  IEnumerable<Ligacao> Ligacoes { get; set; }
        public Boolean? Melhor { get; set; }

        public Rota()
        {
            Melhor = false;
        }

    }
}