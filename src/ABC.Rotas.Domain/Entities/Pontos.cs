﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABC.Rotas.Domain.Entities
{
    public class Ponto
    {        
        public int Id { get; set; }
        public string Nome { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public List<string> Erros { get; set; }

        public Ponto()
        {
        }

    }
}