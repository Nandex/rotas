﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABC.Rotas.Domain.Entities
{
    public class Ligacao
    {
        public int Id { get; set; }
        public int P1_Id { get; set; }
        public int P2_Id { get; set; }
        public List<string> Erros { get; set; }

        public Ligacao()
        {
        }
    }
}