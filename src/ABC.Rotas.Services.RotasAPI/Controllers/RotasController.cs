﻿using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ABC.Rotas.Services.RotasAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RotasController : ApiController
    {
        private IRotaAppService RotaAppService;

        public RotasController(IRotaAppService rotaAppService)
        {
            RotaAppService = rotaAppService;
        }
        
        [Route("api/Rotas/{pontoOrigem}/{pontoDestino}")]
        [HttpGet]
        public IHttpActionResult Get(int pontoOrigem, int pontoDestino)
        {

            var rota = RotaAppService.BuscarRota(pontoOrigem, pontoDestino).Ligacoes;
            if (rota.Count() == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(rota);
            }

        }

    }
}
