﻿using ABC.Rotas.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABC.Rotas.UI.Web.Mappers
{
    public class Mapper
    {
        public static List<SelectListItem> MapToSelectListItem(List<PontoViewModel> pontos)
        {
            var itemSelecione = new SelectListItem { Text = "[Selecione]", Value = "0" };
            var ddl = new SelectList(pontos, "Id", "Nome").ToList();
            ddl.Insert(0, itemSelecione);
            return ddl;

        }

    }
}