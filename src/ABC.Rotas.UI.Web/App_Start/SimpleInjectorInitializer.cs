[assembly: WebActivator.PostApplicationStartMethod(typeof(ABC.Rotas.UI.Web.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace ABC.Rotas.UI.Web.App_Start
{
    using System.Reflection;
    using System.Web.Mvc;
    using WebActivator;
    using SimpleInjector;
    using SimpleInjector.Integration.Web.Mvc;
    using ABC.Rotas.Infra.CrossCutting.Ioc;
    using SimpleInjector.Integration.Web;
    
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
            RegistradorIoc.RegisterServices(container);
        }
    }
}