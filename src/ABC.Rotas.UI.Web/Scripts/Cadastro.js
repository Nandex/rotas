﻿$(document).delegate('#CreateLigacaoViewModelP1Id', 'change', function () {
    $.ajax({
        url: "../Pontos/GetPontosSemLigacao",
        type: 'POST',
        dataType: 'Json',
        data: { Id: $("#CreateLigacaoViewModelP1Id").val() },
        async: false,
        success: function (result) {
            $("#CreateLigacaoViewModelP2Id").empty();
            $("#CreateLigacaoViewModelP2Id").append('<option value>Selecione...</option>');
            $.each(result, function (index, element) {
                $("#CreateLigacaoViewModelP2Id").append('<option value="' + element.Id + '">' + element.Nome + '</option>');
            });
        },
        error: function (result) {
            console.log(result);
        }
    });
});

$(document).delegate('#DeleteLigacaoViewModelP1Id', 'change', function () {
    $.ajax({
        url: "../Pontos/GetPontosComLigacao",
        type: 'POST',
        dataType: 'Json',
        data: { Id: $("#DeleteLigacaoViewModelP1Id").val() },
        async: false,
        success: function (result) {
            $("#DeleteLigacaoViewModelP2Id").empty();
            $("#DeleteLigacaoViewModelP2Id").append('<option value>Selecione...</option>');
            $.each(result, function (index, element) {
                $("#DeleteLigacaoViewModelP2Id").append('<option value="' + element.Id + '">' + element.Nome + '</option>');
            });
        },
        error: function (result) {
            console.log(result);
        }
    });
});

$(document).delegate('#DeletePontoViewModelId', 'change', function () {
    $.ajax({
        url: "../Pontos/GetPontosCoordenadas",
        type: 'POST',
        dataType: 'Json',
        data: { Id: $("#DeletePontoViewModelId").val() },
        async: false,
        success: function (result) {
            $("#coordenadaX").val(result.X);
            $("#coordenadaY").val(result.Y);
        },
        error: function (result) {
            console.log(result);
        }
    });
});

$(document).delegate('#EditPontoViewModelId', 'change', function () {
    $.ajax({
        url: "../Pontos/GetPontosCoordenadas",
        type: 'POST',
        dataType: 'Json',
        data: { Id: $("#EditPontoViewModelId").val() },
        async: false,
        success: function (result) {
            $("#EditPontoViewModelNome").val(result.Nome);
            $("#EditPontoViewModelX").val(result.X);
            $("#EditPontoViewModelY").val(result.Y);
        },
        error: function (result) {
            console.log(result);
        }
    });
});
