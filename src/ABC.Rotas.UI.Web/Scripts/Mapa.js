﻿var config = {
    escala: 10,
    gameWidth: $("#coordenadasCanvas").width(),
    gameHeight: $("#coordenadasCanvas").height(),
    cellWidth: 2,
    lineWidth: 0.5,
    ajusteEixoCentralX: $("#coordenadasCanvas").width() / 2,
    ajusteEixoCentralY: $("#coordenadasCanvas").height() / 2,
    scoreTextStyle: '20px Verdana',
    colors: {
        background: 'white',
        boardBorder: 'green',
        eixos: 'black',
        routes: 'red',
        coordenadas: {
            text: 'black',
            lines: 'blue'
        }
    },
    context: document.getElementById("coordenadasCanvas").getContext('2d')
};

function initView(config) {
    PaintEixoCartesiano();
    PaintGrade();
    PaintCoordenadas(GetPontos());
    PaintLigacoes(GetLinks(), config.lineWidth, config.colors.coordenadas.lines);
};

$(document).ready(function () {
    initView(config);
    return;
});

function GetPontos() {
    var listPoints;
    $.ajax({
        url: "../Pontos/GetPontos",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (result) {
            listPoints = result;
        },
        error: function (result) {
            console.log(result);
        }
    });
    return listPoints;
}

function GetLinks() {
    var listLinks;
    $.ajax({
        url: "../Ligacoes/GetLigacoes",
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (result) {
            listLinks = result;
        },
        error: function (result) {
            console.log(result);
        }
    });
    return listLinks;
}

function PaintLigacoes(ligacoes, lineWidth, color) {

    for (indice in ligacoes) {
        config.context.lineWidth = lineWidth;
        config.context.strokeStyle = color;
        config.context.beginPath();
        PaintScore(ligacoes[indice].P1.Nome, ligacoes[indice].P1.X * config.escala, ligacoes[indice].P1.Y * config.escala * (-1));
        config.context.lineTo((ligacoes[indice].P1.X * config.escala) + (config.gameWidth / 2), (config.gameHeight / 2) + (ligacoes[indice].P1.Y * config.escala * (-1)));
        config.context.lineTo((ligacoes[indice].P2.X * config.escala) + (config.gameWidth / 2), (config.gameHeight / 2) + (ligacoes[indice].P2.Y * config.escala * (-1)));
        config.context.stroke();
    }
    config.context.lineWidth = 0.5;
}

function PaintScore(score, x, y) {
    x += config.ajusteEixoCentralX;
    y += config.ajusteEixoCentralY;
    config.context.fillStyle = config.colors.coordenadas.text;
    config.context.fillText(score, x, y);
}

function PaintEixoCartesiano() {
    config.context.fillStyle = config.colors.background;
    config.context.fillRect(0, 0, config.gameWidth, config.gameHeight);
    config.context.strokeStyle = config.colors.boardBorder;
    config.context.strokeRect(0, 0, config.gameWidth, config.gameHeight);
}

function PaintGrade() {
    var n = 0;
    var strokeStyle = config.context.strokeStyle;

    for (n = 0; n <= config.gameHeight; n += config.escala) {
        if (n == config.ajusteEixoCentralY) {
            config.context.strokeStyle = config.colors.eixos;
            config.context.lineWidth = 1;
        }
        else {
            config.context.strokeStyle = strokeStyle;
            config.context.lineWidth = 0.5;
        }
        config.context.beginPath();
        config.context.moveTo(0, n);
        config.context.lineTo(config.gameWidth, n);
        config.context.stroke();
    }

    for (n1 = 0; n1 <= config.gameWidth; n1 += config.escala) {
        if (n1 == config.ajusteEixoCentralX) {
            config.context.strokeStyle = config.colors.eixos;
            config.context.lineWidth = 1;
        }
        else {
            config.context.strokeStyle = strokeStyle;
            config.context.lineWidth = 0.5;
        }
        config.context.beginPath();
        config.context.moveTo(n1, 0);
        config.context.lineTo(n1, config.gameHeight);
        config.context.stroke();
    }
}

function PaintCoordenadas(pontos) {
    config.context.strokeStyle = config.colors.coordenadas.lines;
    config.context.beginPath();
    for (indice in pontos) {
        var coordenadasCell = pontos[indice];
        coordenadasCell.X = coordenadasCell.X * config.escala;
        coordenadasCell.Y = coordenadasCell.Y * config.escala;
        coordenadasCell.Id = coordenadasCell.Id;
        PaintScore(coordenadasCell.Nome, coordenadasCell.X, coordenadasCell.Y * (-1));
    }
    config.context.stroke();
}

function PaintCell(x, y) {
    x += config.ajusteEixoCentralX;
    y += config.ajusteEixoCentralY;
    context.fillRect(x, y, config.cellWidth, config.cellWidth);
    context.strokeRect(x, y, config.cellWidth, config.cellWidth);
}


