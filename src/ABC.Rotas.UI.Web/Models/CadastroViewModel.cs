﻿using ABC.Rotas.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABC.Rotas.UI.Web.Models
{
    public class CadastroViewModel
    {
        public List<string> Erros { get; set; }
        public PontoViewModel CreatePontoViewModel { get; set; }
        public PontoViewModel EditPontoViewModel { get; set; }        
        public int DeletePontoViewModelId { get; set; }
        [Display(Name = "Ponto origem")]
        public int CreateLigacaoViewModelP1Id { get; set; }
        [Display(Name = "Ponto destino")]
        public int CreateLigacaoViewModelP2Id { get; set; }
        public LigacaoViewModel CreateLigacaoViewModel { get; set; }
        [Display(Name = "Ponto origem")]
        public int DeleteLigacaoViewModelP1Id { get; set; }
        [Display(Name = "Ponto destino")]
        public int DeleteLigacaoViewModelP2Id { get; set; }
        public LigacaoViewModel DeleteLigacaoViewModel { get; set; }

        public CadastroViewModel()
        {
            Erros = new List<string>();
            CreatePontoViewModel = new PontoViewModel();
            EditPontoViewModel = new PontoViewModel();
            CreateLigacaoViewModel = new LigacaoViewModel();
            DeleteLigacaoViewModel = new LigacaoViewModel();
        }
    }
}