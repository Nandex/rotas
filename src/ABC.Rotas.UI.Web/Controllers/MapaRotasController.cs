﻿using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.UI.Web.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABC.Rotas.UI.Web.Controllers
{
    public class MapaRotasController : Controller
    {
        private readonly IRotaAppService AppRotaService;
        private readonly IPontoAppService AppPontoService;

        public MapaRotasController(IRotaAppService appRotaService, IPontoAppService appPontoService)
        {
            AppRotaService = appRotaService;
            AppPontoService = appPontoService;
        }

        public ActionResult Index()
        {
            InicializaDropdowns();
            return View();
        }

        //[HttpPost]
        //public JsonResult GetRotas(int pontoOrigem, int pontoDestino)
        //{
        //    var retorno = AppRotaService.BuscarRota(pontoOrigem, pontoDestino);
        //    return Json(retorno.Ligacoes, JsonRequestBehavior.AllowGet);
        //}

        private void InicializaDropdowns()
        {
            var pontos = Mapper.MapToSelectListItem(AppPontoService.PontosComLigacao().ToList());
            ViewBag.pontoOrigem = pontos;
            ViewBag.pontoDestino = pontos;
        }

    }
}