﻿using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.UI.Web.Mappers;
using ABC.Rotas.UI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABC.Rotas.UI.Web.Controllers
{
    public class LigacoesController : Controller
    {
        private readonly ILigacaoAppService AppLigacaoService;
        private readonly IPontoAppService AppPontoService;
        
        public LigacoesController(ILigacaoAppService appLigacaoService, IPontoAppService appPontoService)
        {
            AppLigacaoService = appLigacaoService;
            AppPontoService = appPontoService;
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CadastroViewModel CadastroViewModel)
        {
            if (ModelState.IsValid)
            {
                LigacaoViewModel ligacaoViewModel = new LigacaoViewModel { 
                    P1_Id = CadastroViewModel.CreateLigacaoViewModelP1Id,
                    P2_Id = CadastroViewModel.CreateLigacaoViewModelP2Id,
                };                
                CadastroViewModel.CreateLigacaoViewModel = AppLigacaoService.Adicionar(ligacaoViewModel);
                if (CadastroViewModel.CreateLigacaoViewModel.Erros.Count == 0)
                    return RedirectToAction("Index");
            }
            else
            {
                ModelState.Values.ToList().ForEach(x =>
                {
                    x.Errors.ToList().ForEach(e =>
                    {
                        CadastroViewModel.CreateLigacaoViewModel.Erros.Add(e.ErrorMessage);
                    });
                });                
            }

            InicializaDropdowns();

            return View("../Pontos/Index", CadastroViewModel);
        }

        [ValidateAntiForgeryToken]
        public ActionResult Delete(CadastroViewModel cadastroViewModel)
        {
            if (ModelState.IsValid)
            {
                if (AppLigacaoService.Remover(cadastroViewModel.DeleteLigacaoViewModelP1Id, cadastroViewModel.DeleteLigacaoViewModelP2Id) > 0)
                    return RedirectToAction("Index", "Pontos");
            }

            InicializaDropdowns();

            cadastroViewModel.Erros.Add("Nao foi possível excluir a ligação especificada.");

            return View("../Pontos/Index", cadastroViewModel);
        }

        public JsonResult GetLigacoes()
        {
            List<LigacaoViewModel> retorno = new List<LigacaoViewModel>();
            AppLigacaoService.ObterTodos().ToList().ForEach(x =>
            {
                retorno.Add(new LigacaoViewModel
                {
                    Id = x.Id,
                    P1 = AppPontoService.ObterPorId(x.P1_Id),
                    P2 = AppPontoService.ObterPorId(x.P2_Id)
                });
            });

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public void InicializaDropdowns()
        {
            var pontos = Mapper.MapToSelectListItem(AppPontoService.ObterTodos().ToList());
            ViewData["EditPontoViewModel.Id"] = pontos;
            ViewBag.CreateLigacaoViewModelP1Id = pontos;

            ViewBag.DeletePontoViewModelId = Mapper.MapToSelectListItem(AppPontoService.PontosSemLigacao().ToList());
            ViewBag.DeleteLigacaoViewModelP1Id = Mapper.MapToSelectListItem(AppPontoService.PontosComLigacao().ToList());

            pontos = Mapper.MapToSelectListItem(new List<PontoViewModel>());
            ViewBag.CreateLigacaoViewModelP2Id = pontos;
            ViewBag.DeleteLigacaoViewModelP2Id = pontos;
        }

    }
}
