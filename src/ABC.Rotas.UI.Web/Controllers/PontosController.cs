﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.UI.Web.Models;
using ABC.Rotas.Application.Servicos;
using ABC.Rotas.Application.AutoMapper;
using ABC.Rotas.UI.Web.Mappers;

namespace ABC.Rotas.UI.Web.Controllers
{
    public class PontosController : Controller
    {
        private readonly IPontoAppService AppPontoService;

        public PontosController(IPontoAppService appPontoService)
        {
            AppPontoService = appPontoService;
        }

        public ActionResult Index()
        {
            InicializaDropdowns();
            return View(new CadastroViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CadastroViewModel CadastroViewModel)
        {            
            if (ModelState.IsValid)
            {                
                CadastroViewModel.CreatePontoViewModel = AppPontoService.Adicionar(CadastroViewModel.CreatePontoViewModel);
                if (CadastroViewModel.CreatePontoViewModel.Erros.Count == 0)
                    return RedirectToAction("Index");
            }
            else
            {
                ModelState.Values.ToList().ForEach(x =>
                {
                    x.Errors.ToList().ForEach(e =>
                    {
                        CadastroViewModel.CreatePontoViewModel.Erros.Add(e.ErrorMessage);
                    });
                });                
            }

            InicializaDropdowns();

            return View("Index", CadastroViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CadastroViewModel CadastroViewModel)
        {            
            if (ModelState.IsValid)
            {                
                CadastroViewModel.EditPontoViewModel = AppPontoService.Atualizar(CadastroViewModel.EditPontoViewModel);
                if (CadastroViewModel.EditPontoViewModel.Erros.Count == 0)
                    return RedirectToAction("Index");
            }
            else
            {
                ModelState.Values.ToList().ForEach(x =>
                {
                    x.Errors.ToList().ForEach(e =>
                    {
                        CadastroViewModel.EditPontoViewModel.Erros.Add(e.ErrorMessage);
                    });
                
               });
            }

            InicializaDropdowns();

            return View("Index", CadastroViewModel);
        }


        [ValidateAntiForgeryToken]
        public ActionResult Delete(CadastroViewModel cadastroViewModel)
        {
            if (AppPontoService.Remover(cadastroViewModel.DeletePontoViewModelId) > 0)
                return RedirectToAction("Index");

            InicializaDropdowns();

            cadastroViewModel.Erros.Add("Nao foi possível excluir o ponto especificado");

            return View("Index", cadastroViewModel);
        }

        public JsonResult GetPontos()
        {
            return Json(AppPontoService.ObterTodos(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPontosComLigacao(int Id)
        {
            var lista = new List<PontoViewModel>();
            return Json(AppPontoService.PontosComLigacao(Id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPontosSemLigacao(int Id)
        {
            var lista = new List<PontoViewModel>();
            return Json(AppPontoService.PontosSemLigacao(Id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPontosCoordenadas(int Id)
        {
            var lista = new List<PontoViewModel>();
            return Json(AppPontoService.ObterPorId(Id), JsonRequestBehavior.AllowGet);
        }

        public void InicializaDropdowns()
        {
            var pontos = Mapper.MapToSelectListItem(AppPontoService.ObterTodos().ToList());
            ViewData["EditPontoViewModel.Id"] = pontos;
            ViewBag.CreateLigacaoViewModelP1Id = pontos;

            ViewBag.DeletePontoViewModelId = Mapper.MapToSelectListItem(AppPontoService.PontosSemLigacao().ToList());
            ViewBag.DeleteLigacaoViewModelP1Id = Mapper.MapToSelectListItem(AppPontoService.PontosComLigacao().ToList());

            pontos = Mapper.MapToSelectListItem(new List<PontoViewModel>());
            ViewBag.CreateLigacaoViewModelP2Id = pontos;
            ViewBag.DeleteLigacaoViewModelP2Id = pontos;
        }

    }
}
