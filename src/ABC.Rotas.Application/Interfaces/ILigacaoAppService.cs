﻿using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.Domain.Entities;
using System.Collections.Generic;

namespace ABC.Rotas.Application.Interfaces
{
    public interface ILigacaoAppService
    {
        LigacaoViewModel Adicionar(LigacaoViewModel objeto);

        LigacaoViewModel ObterPorId(int Id);

        IEnumerable<LigacaoViewModel> ObterTodos();

        //IEnumerable<T> Buscar(System.Linq.Expressions.Expression<Func<T, bool>> predicate);

        //LigacaoViewModel Atualizar(LigacaoViewModel objeto);

        int Remover(int Id1, int Id2);

    }
}
