﻿using ABC.Rotas.Application.ViewModels;
using System;
using System.Collections.Generic;


namespace ABC.Rotas.Application.Interfaces
{
    public interface IPontoAppService
    {
        PontoViewModel Adicionar(PontoViewModel objeto);

        PontoViewModel ObterPorId(int Id);

        IEnumerable<PontoViewModel> ObterTodos();

        //IEnumerable<T> Buscar(System.Linq.Expressions.Expression<Func<T, bool>> predicate);

        PontoViewModel Atualizar(PontoViewModel objeto);

        int Remover(int Id);

        IEnumerable<PontoViewModel> PontosComLigacao();
        IEnumerable<PontoViewModel> PontosComLigacao(int Id);
        IEnumerable<PontoViewModel> PontosSemLigacao();
        IEnumerable<PontoViewModel> PontosSemLigacao(int Id);

    }
}
