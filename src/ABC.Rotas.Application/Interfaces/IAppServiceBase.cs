﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Application.Interfaces
{
    public interface IAppServiceBase<T> where T : class
    {
        T Adicionar(T objeto);

        T ObterPorId(int Id);

        IEnumerable<T> ObterTodos();

//        IEnumerable<T> Buscar(System.Linq.Expressions.Expression<Func<T, bool>> predicate);

        T Atualizar(T objeto);

        int Remover(int Id);

        //void Dispose();

    }
}
