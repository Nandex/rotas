﻿using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.Domain.Entities;

namespace ABC.Rotas.Application.Interfaces
{
    public interface IRotaAppService
    {
        RotaViewModel BuscarRota(int origemId, int destinoId);
    }
}
