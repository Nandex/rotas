﻿using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.Domain.Entities;
using AutoMapper;

namespace ABC.Rotas.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Ponto, PontoViewModel>();
            Mapper.CreateMap<Ligacao, LigacaoViewModel>();
            Mapper.CreateMap<Rota, RotaViewModel>();
        }

    }
}