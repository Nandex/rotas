﻿using AutoMapper;
using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.Domain.Entities;

namespace ABC.Rotas.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<PontoViewModel, Ponto>();
            Mapper.CreateMap<LigacaoViewModel, Ligacao>();
            Mapper.CreateMap<RotaViewModel, Rota>();
        }

    }
}