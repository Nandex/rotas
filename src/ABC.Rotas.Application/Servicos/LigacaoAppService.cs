﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Domain.Interfaces.Services;
using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Application.ViewModels;
using AutoMapper;
using ABC.Rotas.Domain.Validations;
using ABC.Rotas.Domain.Validations.LigacaoValidations;

namespace ABC.Rotas.Application.Servicos
{
    public class LigacaoAppService : ILigacaoAppService
    {
        private readonly ILigacaoService _ligacaoService;
        private readonly IPontoService _pontoService;

        public LigacaoAppService(ILigacaoService ligacaoService, IPontoService pontoService)
        {
            _ligacaoService = ligacaoService;
            _pontoService = pontoService;
        }

        public LigacaoViewModel Adicionar(LigacaoViewModel objeto)
        {
            return Mapper.Map<LigacaoViewModel>(_ligacaoService.Adicionar(Mapper.Map<Ligacao>(objeto)));
        }

        public LigacaoViewModel ObterPorId(int Id)
        {
            var retorno = Mapper.Map<LigacaoViewModel>(_ligacaoService.ObterPorId(Id));
            retorno.P1 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(retorno.P1_Id));
            retorno.P2 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(retorno.P2_Id));
            return retorno;
        }

        public IEnumerable<LigacaoViewModel> ObterTodos()
        {
           var retorno = Mapper.Map<IEnumerable<LigacaoViewModel>>(_ligacaoService.ObterTodos());
           retorno.ToList().Each(x =>
           {
               x.P1 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(x.P1_Id));
               x.P2 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(x.P2_Id));
           });
           return retorno;
        }

        public int Remover(int Id1, int Id2)
        {
            return _ligacaoService.Remover(Id1, Id2);
        }

    }

}
