﻿using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Domain.Interfaces.Services;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Application.Servicos
{
    public class RotaAppService : IRotaAppService
    {
        private readonly ILigacaoService _ligacaoService;
        private readonly IPontoService _pontoService;

        public RotaAppService(ILigacaoService ligacaoService, IPontoService pontoService)
        {
            _ligacaoService = ligacaoService;
            _pontoService = pontoService;
        }


        public RotaViewModel BuscarRota(int origemId, int destinoId)
        {
            PontoViewModel origem = Mapper.Map<PontoViewModel>(_pontoService.ObterTodos().Single(x => x.Id == origemId));
            PontoViewModel destino = Mapper.Map<PontoViewModel>(_pontoService.ObterTodos().Single(x => x.Id == destinoId));

            RotaViewModel retorno = new RotaViewModel();

            var pontos = _pontoService.ObterTodos();

            var ligacoes = _ligacaoService.ObterTodos();


            var ligacao = Mapper.Map<List<LigacaoViewModel>>(ligacoes.Where(x => x.P1_Id == origem.Id && x.P2_Id == destino.Id).ToList());
            if (ligacao.Count > 0)
            {
                ligacao.ToList().Each(x =>
                {
                    x.P1 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(x.P1_Id));
                    x.P2 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(x.P2_Id));
                });
                retorno.Ligacoes = ligacao;
                return retorno;
            }
                
            ligacao = Mapper.Map<List<LigacaoViewModel>>(ligacoes.Where(x => x.P2_Id == origem.Id && x.P1_Id == destino.Id).ToList());
            if (ligacao.Count > 0)
            {
                ligacao.ToList().Each(x =>
                {
                    x.P1 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(x.P1_Id));
                    x.P2 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(x.P2_Id));
                });
                retorno.Ligacoes = ligacao;
                return retorno;
            }
                
            RotaViewModel[] Rotas = new RotaViewModel[1000];
            Boolean continua = true;

            int iRotaIni = 0;
            int iRotaFim = 0;
            int quantidade = 0;

            List<string> caminho = new List<string>();
            try
            {
                while (continua)
                {
                    PontoViewModel[] PontosOrigemAux;
                    PontosOrigemAux = InicializaPontosOrigem(origem, Rotas, iRotaIni, iRotaFim);

                    iRotaIni = iRotaFim;
                    quantidade = 0;

                    for (int i = 0; i < PontosOrigemAux.Count(); i++)
                    {
                        var lx = Mapper.Map<List<LigacaoViewModel>>(ligacoes.Where(x => x.P1_Id == PontosOrigemAux[i].Id || x.P2_Id == PontosOrigemAux[i].Id).ToList());
                        int i1 = 0;
                        LigacaoViewModel[] lstLigacoes = new LigacaoViewModel[lx.Count()];
                        lx.ForEach(x =>
                        {
                            if (!LigacaoJaExisteAntriormente(Rotas, iRotaFim, x))
                            {
                                x.PDest = PontoInversoDaLigacao(PontosOrigemAux[i], x);
                                x.POrig = PontosOrigemAux[i];
                                lstLigacoes[i1] = x;
                                i1++;
                            }
                        });

                        for (i1 = 0; i1 < lstLigacoes.Count(); i1++)
                        {
                            if (lstLigacoes[i1] != null)
                            {
                                RotaViewModel rota = new RotaViewModel();
                                rota.Ligacoes = new List<LigacaoViewModel>();
                                rota.Ligacoes.Add(lstLigacoes[i1]);

                                if (lstLigacoes[i1].P1_Id == destino.Id || lstLigacoes[i1].P2_Id == destino.Id)
                                {
                                    rota.Melhor = true;
                                    continua = false;
                                }
                                Rotas[iRotaFim] = rota;
                                iRotaFim++;
                                quantidade++;
                            }
                        }
                    }

                    if (quantidade == 0) continua = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            var arrayRotas = PercorreLigacoes(Rotas, Rotas.ToList().Where(x => x.Melhor).FirstOrDefault().Ligacoes.Single(), destino, origem);

            retorno.Ligacoes = new List<LigacaoViewModel>();

            arrayRotas.ToList().ForEach(x =>
            {
                retorno.Ligacoes.Add(x.Ligacoes.FirstOrDefault());
            });

            return retorno;

        }

        private PontoViewModel[] InicializaPontosOrigem(PontoViewModel origem, RotaViewModel[] Rotas, int iRotaIni, int iRotaFim)
        {
            PontoViewModel[] PontosOrigemAux;

            if (iRotaFim != iRotaIni)
                PontosOrigemAux = new PontoViewModel[iRotaFim - iRotaIni];
            else
            {
                PontosOrigemAux = new PontoViewModel[1];
                PontosOrigemAux[0] = origem;
            }

            int indice = 0;
            for (int i = iRotaIni; i < iRotaFim; i++)
            {
                PontosOrigemAux[indice] = Rotas[i].Ligacoes.FirstOrDefault().PDest;
                indice++;
            }
            return PontosOrigemAux;
        }

        private RotaViewModel[] PercorreLigacoes(RotaViewModel[] rotas, LigacaoViewModel ligacao, PontoViewModel destino, PontoViewModel origem)
        {
            List<LigacaoViewModel> lstLigacao = new List<LigacaoViewModel>();
            lstLigacao.Add(ligacao);
            Boolean continua = true;
            List<RotaViewModel> lstRota = new List<RotaViewModel>();
            lstRota.Add(new RotaViewModel { Ligacoes = lstLigacao });
            var aRotas = rotas.Where(x => x != null).ToArray();
            int indice = aRotas.Count() - 1;
            while (continua)
            {
                for (int i = indice; i >= 0; i--)
                {
                    indice = i;
                    if (aRotas[i].Ligacoes.FirstOrDefault().PDest.Id == ligacao.POrig.Id)
                    {
                        lstRota.Add(aRotas[i]);
                        ligacao = new LigacaoViewModel { PDest = aRotas[i].Ligacoes.FirstOrDefault().PDest, POrig = aRotas[i].Ligacoes.FirstOrDefault().POrig };
                        if (ligacao.POrig.Id == origem.Id)
                            continua = false;
                        i = -1;
                    }
                }
            }
            return lstRota.ToArray();
        }

        private PontoViewModel PontoInversoDaLigacao(PontoViewModel ponto, LigacaoViewModel ligacao)
        {
            ligacao.P1 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(ligacao.P1_Id));
            ligacao.P2 = Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(ligacao.P2_Id));
            if (ponto.Id == ligacao.P1_Id)
            {
                return ligacao.P2;
            }
            else if (ponto.Id == ligacao.P2_Id)
            {
                return ligacao.P1;
            }
            else
            {
                return null;
            }
        }

        private Boolean LigacaoJaExisteAntriormente(RotaViewModel[] rotas, int indice, LigacaoViewModel ligacao)
        {
            Boolean retorno = false;
            try
            {
                for (int i = indice; i >= 0; i--)
                {
                    if (rotas[i] != null)
                        if ((rotas[i].Ligacoes.FirstOrDefault().P1_Id == ligacao.P1_Id && rotas[i].Ligacoes.FirstOrDefault().P2_Id == ligacao.P2_Id) ||
                            (rotas[i].Ligacoes.FirstOrDefault().P2_Id == ligacao.P1_Id && rotas[i].Ligacoes.FirstOrDefault().P1_Id == ligacao.P2_Id))
                            retorno = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retorno;
        }

    }
}
