﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Domain.Interfaces.Services;

namespace ABC.Rotas.Application.Servicos
{
    public class AppServiceBase<T> : IAppServiceBase<T> where T : class
    {
        private readonly IBaseService<T> _serviceBase;

        public AppServiceBase(IBaseService<T> serviceBase)
        {
            _serviceBase = serviceBase;
        }

        public T Adicionar(T objeto)
        {
            return _serviceBase.Adicionar(objeto);
        }

        public T ObterPorId(int Id)
        {
            return _serviceBase.ObterPorId(Id);
        }

        public IEnumerable<T> ObterTodos()
        {
            return _serviceBase.ObterTodos();
        }

        //public IEnumerable<T> Buscar(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        //{
        //    return _serviceBase.Buscar(predicate);
        //}

        public T Atualizar(T objeto)
        {
            return _serviceBase.Atualizar(objeto);
        }

        public int Remover(int Id)
        {
            return _serviceBase.Remover(Id);
        }

        //public void Dispose()
        //{
        //    Dispose();
        //}
    }
}
