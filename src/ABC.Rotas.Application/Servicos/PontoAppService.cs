﻿using ABC.Rotas.Application.Interfaces;
using ABC.Rotas.Application.ViewModels;
using ABC.Rotas.Domain.Entities;
using ABC.Rotas.Domain.Interfaces.Services;
using ABC.Rotas.Domain.Validations;
using ABC.Rotas.Domain.Validations.PontoValidations;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Application.Servicos
{
    public class PontoAppService : IPontoAppService
    {
        private readonly IPontoService _pontoService;
        private readonly ILigacaoService _ligacaoService;

        public PontoAppService(IPontoService pontoService, ILigacaoService ligacaoService)
        {
            _pontoService = pontoService;
            _ligacaoService = ligacaoService;
        }

        public PontoViewModel Adicionar(PontoViewModel objeto)
        {
            return Mapper.Map<PontoViewModel>(_pontoService.Adicionar(Mapper.Map<Ponto>(objeto)));
        }

        public IEnumerable<PontoViewModel> ObterTodos()
        {
            return Mapper.Map<IEnumerable<PontoViewModel>>(_pontoService.ObterTodos());
        }

        public PontoViewModel ObterPorId(int Id)
        {
            return Mapper.Map<PontoViewModel>(_pontoService.ObterPorId(Id));
        }

        public PontoViewModel Atualizar(PontoViewModel objeto)
        {
            return Mapper.Map<PontoViewModel>(_pontoService.Atualizar(Mapper.Map<Ponto>(objeto)));
        }

        public int Remover(int Id)
        {
            if (PontosComLigacao(Id).Count() > 0)
                return -1;
            return _pontoService.Remover(Id);
        }

        public IEnumerable<PontoViewModel> PontosComLigacao()
        {
            var retorno = new List<PontoViewModel>();
            _pontoService.ObterTodos().Each(x =>
            {
                if (_ligacaoService.ObterTodos().Where(l => l.P1_Id == x.Id || l.P2_Id == x.Id).Count() > 0)
                {
                    retorno.Add(Mapper.Map<PontoViewModel>(x));
                }                    
            });
            return retorno;
        }
        public IEnumerable<PontoViewModel> PontosComLigacao(int Id)
        {
            var retorno = new List<PontoViewModel>();
            _pontoService.ObterTodos().Each(x =>
            {
                if (_ligacaoService.ObterTodos().Where(l => (l.P1_Id == x.Id && l.P2_Id == Id) || (l.P2_Id == x.Id && l.P1_Id == Id)).Count() > 0)
                {
                    retorno.Add(Mapper.Map<PontoViewModel>(x));
                }
            });
            return retorno;
        }

        public IEnumerable<PontoViewModel> PontosSemLigacao()
        {
            var retorno = new List<PontoViewModel>();
            _pontoService.ObterTodos().Each(x =>
            {
                if (_ligacaoService.ObterTodos().Where(l => l.P1_Id == x.Id || l.P2_Id == x.Id).Count() == 0)
                {
                    retorno.Add(Mapper.Map<PontoViewModel>(x));
                }
            });
            return retorno;
        }
        public IEnumerable<PontoViewModel> PontosSemLigacao(int Id)
        {
            var retorno = new List<PontoViewModel>();
            _pontoService.ObterTodos().Each(x =>
            {
                if (_ligacaoService.ObterTodos().Where(l => (l.P1_Id == x.Id && l.P2_Id == Id) || (l.P2_Id == x.Id && l.P1_Id == Id)).Count() == 0)
                {
                    retorno.Add(Mapper.Map<PontoViewModel>(x));
                }
            });
            return retorno;
        }

    }
}
