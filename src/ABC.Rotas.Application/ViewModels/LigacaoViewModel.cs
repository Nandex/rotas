﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ABC.Rotas.Application.ViewModels
{
    public class LigacaoViewModel
    {
        public List<string> Erros { get; set; }
        public int Id { get; set; }
        public int P1_Id { get; set; }
        public int P2_Id { get; set; }
        public PontoViewModel P1 { get; set; }
        public PontoViewModel P2 { get; set; }
        public PontoViewModel PDest { get; set; }
        public PontoViewModel POrig { get; set; }
        public PontoViewModel Sentido { get; set; }

        public double Distancia()
        {
            return Math.Sqrt(Math.Pow((P1.X - P2.X), 2) - Math.Pow((P1.Y - P2.Y), 2));
        }

        public LigacaoViewModel()
        {
            Erros = new List<string>();
        }

    }
}
