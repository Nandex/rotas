﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Rotas.Application.ViewModels
{
    public class RotaViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public List<LigacaoViewModel> Ligacoes { get; set; }
        public Boolean Melhor { get; set; }

        public double Distancia()
        {
            return Ligacoes.Sum(x => x.Distancia());
        }


    }
}
