﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace ABC.Rotas.Application.ViewModels
{
    public class PontoViewModel
    {
        public List<string> Erros { get; set; }
        public int Id { get; set; }
        [Required(ErrorMessage = "Informe um nome para esse ponto.")]
        [MaxLength(10, ErrorMessage="O nome do ponto deve ter no máximo 10 caracteres")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Informe um valor numérico para a coordenanada X.")]
        [Range(-59, 59, ErrorMessage = "A coordenada {0} deve estar entre {1} e {2}.")]
        public int X { get; set; }
        [Required(ErrorMessage = "Informe um valor numérico para a coordenanada Y.")]
        [Range(-59, 59, ErrorMessage = "A coordenada {0} deve estar entre {1} e {2}.")]
        public int Y { get; set; }

        public PontoViewModel()
        {
            Erros = new List<string>();
        }

    }
}
